<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatDirectoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directory_dataentry', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->integer('category_id');
            $table->string('address')->nullable();
            $table->string('street')->nullable();
            $table->string('area1')->nullable();
            $table->string('area2')->nullable();
            $table->string('postalcode',false,true)->nullable();
            $table->bigInteger('mobile_no')->nullable();
            $table->bigInteger('businessno1')->nullable();
            $table->bigInteger('businessno2')->nullable();  
            $table->bigInteger('fax')->nullable();
            $table->string('email_id')->nullable();
            $table->string('website')->nullable();
            $table->string('contactperson')->nullable();
            $table->bigInteger('contactno')->nullable();
            $table->bigInteger('tollfree')->nullable();
            $table->integer('status');
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('directory_dataentry');
    }
}
