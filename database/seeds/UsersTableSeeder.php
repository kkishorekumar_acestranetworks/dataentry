<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->delete();
        $users = array(array('id' => '1','name' => 'Admmin','email'=>'ace@gmail.com','password'=>'Acestra@123'));
        DB::table('users')->insert($users);
    }
}
