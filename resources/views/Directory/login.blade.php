<!DOCTYPE html>
<html lang="en" ng-app="bills">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Toronto Directory Dashboard</title>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
  <div class="wrapper">
    <div class="container ">
        <form class="form-signin login" action="/userlogin" method="post">    
          <h2 class="form-signin-heading">Toronto Dataentry Login</h2>
            
             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if (Session::has("success"))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success! </strong> {{ Session::get("success") }}
                </div>
                @endif

                @if (Session::has("failure"))
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Sorry! </strong> {{ Session::get("failure") }}
                </div>
                @endif

          <div class="form-group">
            <label class=" control-label">Email</label>
            <input type="email" class="form-control border"  name="email" placeholder="Enter Email Id" >
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
        </div>

        <div class="form-group">
            <label class=" control-label">Password</label>
            <input type="password" class="form-control border" name="password" placeholder="Name" >
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
    </form>

</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>

