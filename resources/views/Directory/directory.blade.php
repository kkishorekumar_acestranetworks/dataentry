@extends('layout.home')
@section('content')
<div class="content" ng-controller="myCtrl">
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Directory fields for directory Entry Form:</h3>
							</div>

							<div class="panel-body">
								<form  ng-submit="add()"  name="myform"   class="form-horizontal">
									@if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
									@endif

									<div class="form-group">
										<label class="col-md-4 control-label">Company Name:*</label>
										<div class="col-md-6">
											<input type="text" class="form-control border" name="company_name" placeholder="Name" ng-model="directory.company_name"/>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label"> Category:*</label>
										<div class="col-md-6">
											<select class="form-control border" name="category_id" 
											ng-model="directory.category_id" ng-options="data.id as data.category for data in category" >
											<option value="">Select Category</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Address:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="address" placeholder="Door no" ng-model="directory.address"/>	
									</div>
								</div>

								<div  class="form-group">
									<label class="col-md-4 control-label">Street Name:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="street" placeholder="Street name" ng-model="directory.street" />	
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Area 1:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="area1" placeholder="Area" ng-model="directory.area1"/ >
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Area 2:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="area2" placeholder="Area" ng-model="directory.area2" / >
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Zip code:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="postalcode" placeholder="postal code" ng-model="directory.postalcode"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Business Phone Number 1 (Landine):</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="businessno1" placeholder="Enter Business no1" ng-model="directory.businessno1"/ >
										
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Business Phone Number 2 (Landine):</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="businessno2" placeholder="Enter Business no2" ng-model="directory.businessno2"/ >
										
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Mobile Number</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="mobile_no" placeholder="Enter Mobile Number" ng-model="directory.mobile_no"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Fax:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border"  name="fax" placeholder="enter fax no" ng-model="directory.fax"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Email:</label>
									<div class="col-md-6">
										<input type="email" class="form-control border" name="email_id" placeholder="enter email address" ng-model="directory.email_id"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Website:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="website" 
										placeholder="enter website url" ng-model="directory.website"/ >
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Contact Person Name:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="contactperson" placeholder="Person name" ng-model="directory.contactperson"/>		
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Contact Person (Personal No):</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="contactno" placeholder="enter the no" ng-model="directory.contactno"/ >
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label">Tollfree No:</label>
									<div class="col-md-6">
										<input type="text" class="form-control border" name="tollfree" placeholder="Enter Tollfree Number" ng-model="directory.tollfree"/ >
									</div>
								</div>

								<div class="text-center">  
									<input type="submit" value="Submit" class="btn btn-primary" >
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Directory Table:</h3>
						</div>
						<div class="panel-body table-responsive">
							<table class="table table-bordered " >

								<thead>
									<tr>
										<th>Id</th>
										<th>Company Name</th>
										<th>Business Category</th>
										<th>Zipcode</th>
										<th>Email</th>
										<th>Website</th>
										<th>Contact Person</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody>
									<tr dir-paginate="directory in directory_list| itemsPerPage:10">
										<td>@{{directory.id }}</td>
										<td>@{{directory.company_name }}</td>
										<td>@{{directory.business_category_name }}</td>
										<td>@{{directory.postalcode }}</td>
										<td>@{{directory.email_id }}</td>
										<td>@{{directory.website }}</td>
										<td>@{{directory.contactperson }}</td>
										<td>
											<button ng-click="directoryedit(directory.id )" class="btn btn-default fa fa-pencil"></button>
										</td>
									</tr>
								</tbody>
							</table>  
							<dir-pagination-controls
							direction-links="true"
							boundary-links="true" >
						</dir-pagination-controls>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

</div>
</div>

@endsection