 
@extends('layout.home')
@section('content')
<div class="content" ng-controller="myCtrl">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="row">

                 <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Directory Table:</h3>
                    </div>
                    <div class="panel-body table-responsive">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Id</th>
                                <th>Company Name</th>
                                <th>Business Category </th>
                                <th>Bussiness Phone 1</th>
                                <th>Email</th>
                                <th>Website</th>
                                <th>Contact Person</th>
                            </tr>
                        </thead>
                          <tbody>

                  <tr dir-paginate="directory in directory_list| itemsPerPage:10" >
                    <td>@{{directory.id }}</td> 
                    <td>@{{directory.company_name }}</td>

                    <td>@{{directory.business_category_name}}</td>
                   
                    <td>@{{directory.businessno1 }}</td>
                    <td>@{{directory.email_id }}</td>
                    <td>@{{directory.website }}</td>
                    <td>@{{directory.contactperson }}</td>
                </tr>
                </tbody>
                 </table>
                  <dir-pagination-controls
                direction-links="true"
                boundary-links="true" >
            </dir-pagination-controls>
             </div>
         </div>

     </div>

 </div>
</div>

</div>
</div>
@endsection