<!DOCTYPE html>
<html lang="en" ng-app="bills">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Directory Database Dashboard</title>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons-2.0.1/css/ionicons.min.css">
</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-color="" data-image="images/full-screen-image-3.jpg">
        <div class="logo">
            <a href="/home" class="logo-text">
                DIRECTORY DATABASE
            </a>
        </div>
    <div class="logo logo-mini">
      <a href="/home" class="logo-text">
        TD
      </a>
    </div>

      <div class="sidebar-wrapper">
            <div class="user">
                <div class="photo">
                    <img src="images/admin-icon.png" />
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                        Admin
                        <b class="caret"></b>
                    </a>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li><a href="#">My Profile</a></li>
                            <li><a href="#">Edit Profile</a></li>
                            <li><a href="#">Settings</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="/directory">
                        <i class="fa fa-pie-chart"  aria-hidden="true"></i>
                        <p>Directory</p>
                    </a>
                </li>
                 <li class="active">
                    <a href="/directory">
                        <i class="fa fa-eye"  aria-hidden="true"></i>
                        <p>View Directory Data</p>
                    </a>
                </li>
            </ul>
      </div>
    </div>

    <div class="main-panel">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-minimize">
          <button id="minimizeSidebar" class="btn btn-warning btn-fill btn-round btn-icon">
            <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
            <i class="fa fa-navicon visible-on-sidebar-mini"></i>
          </button>
        </div>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-with-icons">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-list"></i>
                <p class="hidden-md hidden-lg">
                  More
                  <b class="caret"></b>
                </p>
              </a>
              <ul class="dropdown-menu dropdown-with-icons">
              
        <!-- <li><a href="#"> <i class="fa fa-sign-in" aria-hidden="true" ></i> Sign Up</a></li>
        <li><a href="#"> <i class="fa fa-sign-in" aria-hidden="true" ></i> Login</a></li> -->
        <li class="nav-item dropdown">
                    <a  class="dropdown-item" href="#" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out" aria-hidden="true" ></i>
                        <p>Logout</p>
                    </a>
                    <form id="logout-form" action="/login" method="POST" style="display: none;">
                          </form>
                </li>
              </ul>
            </li>

          </ul>
        </div>
      </div>
    </nav>

        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>

            </div>
        </div>

@yield('content')

        <footer class="footer">
            <div class="container-fluid">
                <p class="copyright pull-right">
                    &copy; 2017 <a href="#">AcestraNetworks Pvt Ltd..</a>
                </p>
            </div>
        </footer>

    </div>
</div>
   

     <script type="text/javascript" src="js/jquery.min.js"></script>
     <script type="text/javascript" src="js/jquery-ui.min.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/custom.js"></script>
  
</body>
</html>

