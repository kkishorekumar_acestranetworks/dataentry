<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Model\directory;
use App\Model\directorycategory;
use App\Model\user;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


class directoryController extends Controller
{	
	public function login()
	{
		return view('Directory.login');
	}

	public function logout()
	{
		return Redirect::to('/login'); 
	}

	public function createuser()
	{
		
		$name= 'admin';
		$email='ace@gmail.com';
		$password='Acestra@123';

		$user= new user();

		$user->name = $name;
		$user->email = $email;
		$key = 'password to (en/de)crypt';
		$encrypted_password = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $password, MCRYPT_MODE_CBC, md5(md5($key))));
		$user->password = $encrypted_password;
		$user->save();

		if(isset($user->id))
		{
			$response = array('status' => 'success','message'=>'data save successfuly' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'data save Failed' );
		}
		return json_encode($response);
	}

		public function userlogin()
	{	

		/*$rule = array('email' => 'required',
					  'password' => 'required');

		$messages = array('email.required' => 'Email id is required',
						  'password.required' => 'Password is required');

		$validator = Validator::make(Input::all(), $rule,$messages);

		if ( $validator->fails() )
		{
			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		}
		else
		{*/
			$name=Input::get('email');
			$password=Input::get('password');

			$key = 'password to (en/de)crypt';
			$encrypted_password = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $password, MCRYPT_MODE_CBC, md5(md5($key))));

			$user=user::where('name','=',$name)
					->where('password','=',$encrypted_password)
					->first();

			return redirect('/directory_view');
		
		/*}	*/
	}


	public function directory_store()
	{	
		/*$rule = array
			('company_name' => 'required',
			'category' => 'required',
			'street' =>'required',
			'postalcode' => 'required',
			'businessno1' => 'size:10');

		$messages = array('company_name.required' => 'Company Name person name is required',
			'category.required' => 'Category field is required',
			'street.required' => 'Event date field is required',
			'businessno1.required' => 'Business Phone 1 field is required',
			'postalcode.required' => 'Postalcode field is required');

		$validator = Validator::make(Input::all(), $rule,$messages);

		if ( $validator->fails() )
		{
			return Redirect::back()->withErrors($validator)->withInput(Input::all());
		}
		else
		{*/

		$id=Input::get('id');
		$current_date=date("Y-m-d H:i:s");

		if(isset($id) && $id != '') 
		{	

			/*$id=Input::get('id');*/

			$directory=directory::find($id);
			$directory->updated_at = $current_date;
			$directory->save();
		}

		else
		{
			$directory = new directory();
			$directory->created_at = $current_date;
		

		$directory->company_name=Input::get('company_name');
		$directory->category_id=  Input::get('category_id');
		$directory->address=Input::get('address');
		$directory->street= Input::get('street');
		$directory->area1= Input::get('area1');
		$directory->area2= Input::get('area2');
		$directory->postalcode= Input::get('postalcode');
		$directory->businessno1= Input::get('businessno1');
		$directory->businessno2= Input::get('businessno2');
		$directory->mobile_no= Input::get('mobile_no');
		$directory->fax= Input::get('fax');
		$directory->email_id= Input::get('email_id');
		$directory->website= Input::get('website');
		$directory->contactperson= Input::get('contactperson');
		$directory->contactno= Input::get('contactno');
		$directory->tollfree= Input::get('tollfree');
		$directory->status= 1;//Input::get('status');

		
	/*}*/
	}
		if(isset($directory->id))
		{
			$response = array('status' => 'success','message'=>'data save successfuly' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'data save Failed' );
		}
		return Redirect::back();
	}

	public function directory_view()
	{
		$result=directory::all();
		
		return view('Directory.directory',$result);
	} 

	public function directory_list()
	{
		$result= directory::join('directory_category','directory_category.id','directory_dataentry.category_id')
						   ->select('directory_category.category as business_category_name',
						   	'directory_dataentry.id as business_category_id','directory_dataentry.id as directory_id', 'directory_dataentry.*')
						   ->orderBy('id', 'desc')
						   ->get();

		return  json_encode($result);
	}

	public function  directory_edit()
	{	
		$id=Input::get('id');

		$data = directory::find($id);

		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}

	public function directory_category()
	{
		$result=directorycategory::get();

		return  json_encode($result);
	}


	public function viewdirectory()
	{
		$directory['data']=directory::all();

		return view('Directory.viewdirectory',$directory);
	}

	public function viewdirectorydata()
	{
		$result=directory::all();

		return  json_encode($result);
	}



}