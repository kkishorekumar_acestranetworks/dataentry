<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Model\directory;
use App\Model\directorycategory;
use Session;
use App\Model\user;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return view('home');
            $result=directory::all();
        return view('Directory.directory',$result);
    }
    public function directory_store()
    {   
        $rules = array(
            'company_name'=>'required',
            'category_id'=>'required',
            'postalcode'=>'required');

        $messages = [
            'company_name.required' => 'Please Enter Any Company Name.',
            'category_id.required' => 'Please Select any Category.',
            'postalcode.required'=> 'The Postal Code is required.',];

        $validator = Validator::make(Input::all(), $rules,$messages);


        if ( $validator->fails() )
        {  
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        else
        {
            $id= Input::get('id');
            $current_date=date("Y-m-d H:i:s");
            if(isset($id)) 
            {   
                $directory = directory::find($id);
                $directory->company_name=Input::get('company_name');
                $directory->category_id=  Input::get('category_id');
                $directory->address=Input::get('address');
                $directory->street= Input::get('street');
                $directory->area1= Input::get('area1');
                $directory->area2= Input::get('area2');
                $directory->postalcode= Input::get('postalcode');
                $directory->businessno1= Input::get('businessno1');
                $directory->businessno2= Input::get('businessno2');
                $directory->mobile_no= Input::get('mobile_no');
                $directory->fax= Input::get('fax');
                $directory->email_id= Input::get('email_id');
                $directory->website= Input::get('website');
                $directory->contactperson= Input::get('contactperson');
                $directory->contactno= Input::get('contactno');
                $directory->tollfree= Input::get('tollfree');
                $directory->status= 1;//Input::get('status');
                $directory->updated_at=$current_date;
                $directory->save();
                Session::flash("success", "Data Saved Successfuly.");
            }
            else
            {
                $directory = new directory();
                $directory->created_at = $current_date;
                $directory->company_name=Input::get('company_name');
                $directory->category_id=  Input::get('category_id');
                $directory->address=Input::get('address');
                $directory->street= Input::get('street');
                $directory->area1= Input::get('area1');
                $directory->area2= Input::get('area2');
                $directory->postalcode= Input::get('postalcode');
                $directory->businessno1= Input::get('businessno1');
                $directory->businessno2= Input::get('businessno2');
                $directory->mobile_no= Input::get('mobile_no');
                $directory->fax= Input::get('fax');
                $directory->email_id= Input::get('email_id');
                $directory->website= Input::get('website');
                $directory->contactperson= Input::get('contactperson');
                $directory->contactno= Input::get('contactno');
                $directory->tollfree= Input::get('tollfree');
                $directory->status= 1;//Input::get('status');
                $directory->save();
                Session::flash("success", "Data Saved Successfuly.");
            }
        }
        if(isset($directory->id))
        {
            $response = array('status' => 'success','message'=>'data save successfuly' );
        }
        else
        {
            $response = array('status' => 'error','message'=>'data save Failed' );
        }
            return Redirect::back();
    }


    public function directory_view()
    {
        $result=directory::all();
        return view('Directory.directory',$result);
    } 

    public function directory_list()
    {
        $result= directory::join('directory_category','directory_category.id','directory_dataentry.category_id')
        ->select('directory_category.category as business_category_name',
            'directory_dataentry.id as business_category_id','directory_dataentry.id as directory_id', 'directory_dataentry.*')
        ->orderBy('id', 'desc')
        ->get();

        return  json_encode($result);
    }

    public function  directory_edit()
    {   
        $id=Input::get('id');

        $data = directory::find($id);

        if(isset($data))
        {
            $result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
        }
        else
        {
            $result = array('status'=>'error','message'=>'no record found');
        }
        return  json_encode($result);
    }

    public function directory_category()
    {
        $result=directorycategory::get();

        return  json_encode($result);
    }


    public function viewdirectory()
    {
        $directory['data']=directory::all();

        return view('Directory.viewdirectory',$directory);
    }

    public function viewdirectorydata()
    {
        $result=directory::all();

        return  json_encode($result);
    }


}
