<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::any('/','directoryController@login');
// Route::any('/userlogin','directoryController@userlogin');
// Route::any('/logout', 'directoryController@logout'); 
// Route::any('/createuser','directoryController@createuser');
Route::any('/directory_category', 'HomeController@directory_category'); 
Route::any('/directory_view','HomeController@directory_view');
Route::any('/directory_list','HomeController@directory_list');
Route::any('/directory_store','HomeController@directory_store');
Route::any('/directory_edit','HomeController@directory_edit');
Route::any('/viewdirectory','HomeController@viewdirectory');

Auth::routes();
Route::any('/','HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
