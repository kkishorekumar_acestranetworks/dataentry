var app = angular.module('bills', ['ui.bootstrap',,'angularUtils.directives.dirPagination']);


 
// below for angularjs cntrl 
app.controller("AdminCntrl", function ($scope,GenFactory) {
      
      
           GenFactory.get_List("/Admin_list").then(function(result){
           
                      $scope.items = result.list;
                      
              });

            GenFactory.get_List('/get_roles').then(function(result){
                      $scope.roles = result.data;
                      //alert(result.data);
              });


 
});

app.controller('menu_assign_cntrl', function ($scope,GenFactory) {

              GenFactory.get_List('/get_roles').then(function(result){
                      $scope.roles = result.data;
                      //alert(result.data);
              });

              $scope.getRolemenus = function (role) {
                GenFactory.post_List('/get_menus',role).then(function(result){
                    if(result.status == 'success'){
                       angular.forEach(result.data, function(obj, key) {
             
                          if(obj.relation == 'parent'){
                             var main  = "main_menu_"+obj.id; 
                             $scope[main] =  result.checked;
                          }else if (obj.relation == 'child'){
                             var mn = "child_menu_"+obj.id;
                            $scope[mn] =  result.checked;
                            // console.log("relation"+obj.relation+"--"+"id"+obj.id);
                            // console.log($scope.child_menu_3);
                          }
                             
                            
                       });
                     }else{

                     }
                });                     
              }
      
         
              $scope.isChecked = function (id,fun) {
                      var main  = "main_menu_"+id; 
                      if($scope[main] == true){
                      
                      GenFactory.post_List('/get_child_frm_parent',id).then(function(result){
                               angular.forEach(result.data, function(obj, key) {
                                   var mn = "child_menu_"+obj.id;
                                   $scope[mn] = true;

                           }); 
                      });

                           
                      }else{

                          GenFactory.post_List('/get_child_frm_parent',id).then(function(result){
                               angular.forEach(result.data, function(obj, key) {
                                   var mn = "child_menu_"+obj.id;
                                   $scope[mn] = false;

                           }); 
                         });
                      }
                    

              }

             GenFactory.get_List('/get_masters').then(function(result){
                      $scope.masters =  result.data;
                     
              });

             GenFactory.get_List('/get_all_parent_menus').then(function(result){
                      $scope.all_parent_menus = result.data;
                      //alert(result.data);
              });

             $scope.create_parent_menu = function (masterid,new_menu, parent_menu) {
                 

                   GenFactory.post_List_obj('/set_parent_menu',{'masterid':masterid,'new_menu':new_menu,'parent_menu':parent_menu}).then(function(result){
                      $scope.message = result.message;
                      $scope.new_menu = '';
                   });  
                 
                


             }
             
             $scope.create_child_menu = function (masterchildid,menus_id) {
               GenFactory.post_List_obj('/set_child_menu',{'masterchildid':masterchildid,'menus_id':menus_id}).then(function(result){
                      $scope.message = result.message;
                      //alert(result.data);
              });
             }

             $scope.parent_menu = 'choose';

  });


app.controller('menu_manager', function ($scope,GenFactory) {
          
          var get_menu_list = function () {
            GenFactory.get_List('/get_menus_magaer').then(function(result){
                      $scope.menus = result.data;
                      //alert(result.data);
              });
          }   

          var form_clear = function () {
             $scope.menuform = {};
             /*$scope.success = '';
             $scope.error = '';
             $scope.message = '';*/
          }

          var get_parent_list = function () {
            GenFactory.get_List('/get_all_parent_menus').then(function(result){
                      $scope.parents = result.data;
                      
              }); 
          }

         get_menu_list();
         get_parent_list();
         
         $scope.edit_menu = function (id) {
              GenFactory.post_List('/post_menu_edit',id).then(function(result){
                      $scope.menuform = result;
                      if(result.parent !=0){
                         $scope.menuform.Isparent = "yes"; 
                         $scope.menuform.parent = {id:result.parent};
                      }else{
                         $scope.menuform.Isparent = "no"; 
                      }
                      //$scope.Isparent = 
                      //alert(result.data);
              }); 
         }
        $scope.menu_upsert = function () {
          console.log(JSON.stringify($scope.menuform));
            GenFactory.post_List_obj('/post_menu_update',$scope.menuform).then(function(result){
                      if(result.status =='success'){
                        $scope.success = result.message;
                        get_menu_list();
                        form_clear();
                        get_parent_list();
                      }else{
                        $scope.error = result.message;
                        $scope.message = result.message;
                      }
              }); 
        } 

         $scope.form_clear = function () {
            form_clear();
         }

         $scope.status_change_to = function (id,status) {
              GenFactory.post_List_obj('/post_menu_status',{'id':id,'status':status}).then(function(result){
                      $scope.success = 'status updated;'
                      get_menu_list(); 
              }); 
         }
        
         

         

});


app.controller("csvcntrl", function ($scope,GenFactory) {
      
      
           GenFactory.get_List("/uploaed_bills_list").then(function(result){
            $scope.currentPage=1;
                      $scope.items = result.list;
                      
              });

         
});

app.controller("billsHistorycntrl", function ($scope,GenFactory) {
      
      
             var list = function () {
             	GenFactory.get_List("/bills_history_list").then(function(result){
                      $scope.currentPage=1;
                      $scope.items = result.list;
                      
              });
             }
             list();
           $scope.status_change_to = function (id, status) {
              
              GenFactory.post_List_obj('/bill_history_status',{'id':id,'status':status}).then(function(result){
                      $scope.success = 'status updated;'
                      list(); 
              }); 
           }

         
});

app.controller("billsMastercntrl", function ($scope,GenFactory) {
      
      
             var list = function () {
              GenFactory.get_List("/bills_master_list").then(function(result){
                    $scope.currentPage=1;
                      $scope.items = result.list;
                      
              });
             }
             list();
           /*$scope.status_change_to = function (id, status) {
              
              GenFactory.post_List_obj('/bill_master_status',{'id':id,'status':status}).then(function(result){
                      $scope.success = 'status updated;'
                      list(); 
              }); 
           }*/

         
});






