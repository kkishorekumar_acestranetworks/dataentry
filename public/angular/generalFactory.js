app.factory('GenFactory',function($http,$log,$q){
  return{
    get_List:function(url){
    var deferred = $q.defer();
     $http.get(url)
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  },
  post_List:function(url,id){
    var deferred = $q.defer();
     $http.post(url,{'id':id})
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  },
  post_List_obj:function(url,obj){
    var deferred = $q.defer();
     $http.post(url,obj)
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  }

  }

});