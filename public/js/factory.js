  app.factory('userRegisterFactory', function($http,$q,$log) {
  return {
  store: function(url, data) {
      var deferred = $q.defer();
      $http.post( url, data)
      .then(function(data) { 
        deferred.resolve(data);
      }),(function(msg, code) {
        deferred.reject(code);       
      });
      return deferred.promise;
    },
      list: function(url) {
      var deferred = $q.defer();
      $http.get(url )
      .then(function(data) { 
        deferred.resolve(data);
      }),(function(msg, code) {
        deferred.reject(code);       
      });
      return deferred.promise;
    }
  } 

}); 