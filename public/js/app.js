var app = angular.module('myApp', ['angularUtils.directives.dirPagination']);
app.controller('myCtrl', function($scope, $http) {
	
$http.get("directory_category")
  .then(function(result) {
      $scope.category = result.data;
    });

var directory_list =function(){
  $http.get("directory_list")
  .then(function(result) {
  	// console.log(result.data);
      $scope.directory_list = result.data;
      
  });
}
directory_list();

$scope.add = function() {
	 $http.post("directory_store", $scope.directory)
	.then(function(response) {
		$scope.directory = response.data;
		directory_list();
		$scope.directory = "";
		console.log(response.status);
	});
}

$scope.directoryedit = function(id) {
	 $http.post("/directory_edit",{'id' : id})
	.then(function(result){
		$scope.directory = result.data.data;
		console.log(result.data);
	});
};



});
